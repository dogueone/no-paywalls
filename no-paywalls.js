const hn = location.hostname; // .split('.').slice(1).join('.');

switch (hn) {
    case 'www.ilgiorno.it':
        const pGiornoBrief = document.querySelector('div[class="detail-text detail-text--truncated"]');
        const pGiornoHidden = document.querySelector('div[class="detail-text lp_article_content paywall paywall-text"]');
        if (pGiornoHidden) {
            pGiornoHidden.style.display = 'block';
            if (pGiornoBrief) {
                pGiornoBrief.style.display = 'none';
            }
        }
        break;
    case 'espresso.repubblica.it':
        const pEspressoBody = document.querySelector('.premium-pw-active #article-body');
        if (pEspressoBody) {
            pEspressoBody.style.maxHeight = 'none';
        }
        const pEspressoGradient = document.querySelector('#article-body');
        if (pEspressoGradient) {
            pEspressoGradient.id = 'none';
        }
        break;
    case 'mattinopadova.gelocal.it':
        const pMattinoPDArticle = document.querySelector('div#article-body');
        if (pMattinoPDArticle) {
            // console.log('no-pay-walls', pMattinoPDArticle);
            fetch(location.href)
                .then(response => response.text())
                .then(text => {
                    // console.log('no-pay-walls', 'found pSource text');
                    const pSource = document.createElement('html');
                    pSource.innerHTML = text;
                    pMattinoPDArticle.innerHTML = '';
                    pSource
                        .querySelectorAll('#article-body p')
                        .forEach((elem) => {
                            const p = document.createElement('p');
                            p.innerHTML = elem.innerHTML;
                            // console.log('no-pay-walls', 'elem: ' + elem.textContent);
                            pMattinoPDArticle.appendChild(p);
                        });
                    pMattinoPDArticle.style.maxHeight = 'none';
                })
                .catch(error => {
                    console.error('no-pay-walls', error);
                });
        }
        break;
}