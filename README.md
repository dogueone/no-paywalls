# README #

No-Paywalls

### What is this repository for? ###

* This project aims to show articles from online newspapers / blogs unhindered by paywalls. For fair-play I chose not to hide paywalls, just put them below article content.
* It covers a small portion of online newspapers / blogs yet, which I did not find in the much more popular https://github.com/iamadamdev/bypass-paywalls-chrome
* Current version: 1.0.0.5

### How do I get set up? ###

* You can download the component from here https://addons.mozilla.org/firefox/downloads/file/3738448/no_paywalls-1.0.0.5-fx.xpi (it just isn't listed among the public addons to try and avoid getting it fixed)
